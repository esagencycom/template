export default class Hatch {
  url = 'https://hatch.esagency.com/api/';

  constructor({
    country = 'gb',
    prod_type = 'ryzen',
    series = false,
    mpn = false,
    models = false,
    workstation = false,
    order = false,
  } = {}) {
    this.country = country;
    this.prod_type = prod_type;
    this.series = series;
    this.mpn = mpn;
    this.models = models;
    this.workstation = workstation;
    this.order = order;

    if (!this.series && !this.mpn && !this.models) {
      throw new Error("Necessarily indicate the series OR mpn OR models");
    }
  }

  get country() {
    return this._country;
  }

  set country(value) {
    if (['uk', 'en'].includes(value)) {
      return this._country = 'gb';
    }

    return this._country = value;
  }

  get prod_type() {
    return this._prod_type;
  }

  set prod_type(value) {
    if (!['ryzen', 'radeon'].includes(value)) {
      throw new Error('wrong product type!')
    }

    this._prod_type = value;
  }

  urlParams(type = 'products', page, limit) {
    let url = `${this.url}${type}?country=${this.country}&prod_type=${this.prod_type}`;

    if (this.series) {
      url += `&series=${this.series}`;
    }
    if (this.mpn) {
      url += `&mpn=${this.mpn}`;
    }
    if (this.models) {
      url += `&models=${this.models}`;
    }
    if (page) {
      url += `&page=${page}`;
    }
    if (limit) {
      url += `&limit=${limit}`;
    }
    if (this.workstation) {
      url += `&workstation=1`;
    }
    if (this.order) {
      url += `&order=${this.order}`;
    }

    return url;
  }

  async get(type, page, limit) {
    const url = this.urlParams(type, page, limit);
    const result = await fetch(url);
    return await result.json();
  }
}
