import "./Popup.scss";

class Popup {
  static self;

  style = {
    background: "#181818",
    textColor: "#fff",
    buttonHover: "#d54d0d",
    maxWidth: "1200px",
  };

  constructor({
    content = "",
    youtube = false,
    style = {}
  } = {}) {
    if (Popup.self) {
      Popup.self.remove();
    }

    this.content = content;
    this.youtube = youtube;
    this.style = Object.assign(this.style, style);

    this.render();
    this.show();
    this.addEventListener();
  }

  get template() {
    return `
      <div class="widget-popup" style="
          --background: ${this.style.background};
          --buttonHover: ${this.style.buttonHover};
          --textColor: ${this.style.textColor};
          --maxWidth: ${this.style.maxWidth};
        ">
        <div class="widget-popup-wrapper">
          <button class="widget-popup-close"></button>
          ${this.templateContent}
        </div>
      </div>
    `;
  }

  get templateContent() {
    if (this.youtube) {
      return `
        <div class="widget-popup-wrapper_content widget-popup-wrapper_youtube">
          <iframe width="560" height="315" 
            src="https://www.youtube.com/embed/${this.youtube}?rel=0&controls=0&autoplay=1" 
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen>
          </iframe>
        </div>
      `;
    }

    //default
    return `
      <div class="widget-popup-wrapper_content">
        ${this.content}
      </div>
    `;
  }

  render() {
    const element = document.createElement("div");
    element.innerHTML = this.template;

    this.el = element.firstElementChild;
    this.elClose = this.el.querySelector(".widget-popup-close");

    Popup.self = this.el;
  }

  show() {
    document.body.classList.add("widget-popup-open");
    document.body.append(this.el);
  }

  addEventListener() {
    this.elClose.addEventListener("click", (e) => {
      this.remove();
    });
  }

  remove() {
    document.body.classList.remove("widget-popup-open");
    this.el.remove();
  }

  destroy() {
    this.remove();
    this.el = null;
  }
}

export default Popup;
