import "./Buy.scss";
import API from './../../lib/HatchApi';
import Product from './Product';

const i18nVariations = {
  'uk': require('./i18n/uk.json'),
  'ru': require('./i18n/ru.json'),
  'de': require('./i18n/de.json'),
};

export default class Buy {
  color = {
    background: "#181818",
    textColor: "#fff",
    priceColor: "#878787",
    button: "#f26522",
    buttonHover: "#d54d0d",
    buttonColor: "#fff",
  };
  i18nDefault = 'uk';

  constructor({
    color = {},
    params = {},
    lang = this.i18nDefault,
    noEmpty = false, //Не показывать блок "No result"
    page = 1,
    limit = 10,
  } = {}) {
    this.options = {
      color: Object.assign(this.color, color),
      params: params,
      noEmpty: noEmpty,
      page: page,
      limit: limit,
    };

    this.i18n = i18nVariations[lang] || i18nVariations[this.i18nDefault];

    this.render();
    this.updateParams(this.options.params);
    this.addEventListener();
  }

  get template() {
    return `
      <section class="widget-buy" style="
        --background: ${this.options.color.background}; 
        --textColor: ${this.options.color.textColor};
        --priceColor: ${this.options.color.priceColor};
        --button: ${this.options.color.button};
        --buttonHover: ${this.options.color.buttonHover};
        --buttonColor: ${this.options.color.buttonColor};">
      </div>
    `;
  }

  templateMore(next_page, limit) {
    return `
      <div class="widget-buy-add">
        <button class="widget-buy-add_button" 
          data-next_page="${next_page}" 
          data-limit="${limit}">${this.i18n.showMore}
        </button>
      </div>
    `;
  }

  get templateEmpty() {
    return `
      <p class="widget-buy-empty">${this.i18n.empty}</p>
    `;
  }

  render() {
    const element = document.createElement("div");
    element.innerHTML = this.template;

    this.el = element.firstElementChild;
  }

  renderMore(next_page = null, limit) {
    if (this.elMore) {
      this.elMore.remove();
      this.elMore = null;
    }

    if (!next_page) return "";

    const element = document.createElement("div");
    element.innerHTML = this.templateMore(next_page, limit);
    this.elMore = element.firstElementChild;

    return this.elMore;
  }

  async renderProducts(
    remove = false,
    page = this.options.page,
    limit = this.options.limit
  ) {
    if (remove) {
      this.el.innerHTML = "";
    }

    const responce = await this.api.get("products", page, limit);
    const $products = responce.products.map((product) =>
      this.el.append(new Product(product, this.i18n).el)
    );

    this.el.dispatchEvent(
      new CustomEvent("widget-buy-responce", {
        detail: {
          products: responce.products,
        },
        bubbles: false,
      })
    );

    if (responce.products.length <= 0 && !this.options.noEmpty) {
      this.el.innerHTML = this.templateEmpty;
    }

    this.el.append(this.renderMore(responce.next_page, limit));
  }

  /*
   * При изменении страны, серии или npm
   * достаточно передать 1 измененное свойство в updateParams
   */
  updateParams({ country, prod_type, series, models, mpn, workstation, order } = {}) {
    this.options.params.country = country || this.options.params.country;
    this.options.params.prod_type = prod_type || this.options.params.prod_type;
    this.options.params.series = series || this.options.params.series;
    this.options.params.models = models || this.options.params.models;
    this.options.params.mpn = mpn || this.options.params.mpn;
    this.options.params.workstation = workstation ? 1 : 0;
    this.options.params.order = order || this.options.params?.order;

    this.api = new API(this.options.params);
    this.renderProducts(true);
  }

  addEventListener() {
    this.el.addEventListener("click", async (e) => {
      if (e.target.closest(".widget-buy-add_button")) {
        e.target.dataset.loader = true;

        const next_page = e.target.dataset.next_page;
        const limit = e.target.dataset.limit;

        const res = await this.renderProducts(false, next_page, limit);
      }
    });
  }
}