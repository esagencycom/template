export default class Product {
  openClass = 'js-widget-buy-open';
  closeClass = 'js-widget-buy-close';

  constructor(product = {}, i18n) {
    this.product = product;
    this.i18n = i18n;
    this.shops = product.shops ?? [];
    this.render();
    this.addEventListener();
  }

  render() {
    const element = document.createElement('div');
    element.innerHTML = this.template;

    this.el = element.firstElementChild;
  }

  get template() {
    const {photo, name, price, currency, series, mpn} = this.product;

    const image = series.photo || mpn[0].photo || photo;
  
    return `
      <article class="widget-buy-item">
        <img src="${image}" alt="${name}" class="widget-buy-item_image">
        <h3 class="widget-buy-item_name" title="${name}">${name}</h3>
        <footer class="widget-buy-item_footer">
          <strong class="widget-buy-item_price">
            ${this.i18n.price} <br>
            ${this.i18n.from}: ${this.formatPrice(price)} ${currency}
          </strong>
          <button class="widget-buy-item_buy ${this.openClass}">${this.i18n.getYours}</button>
        </footer>
      </article>
    `;
  }

  formatPrice(price) {
    const p = new String(price).split('.');

    if (!p[1]) p.push('00');

    return `${p[0]}.<sup class="widget-buy-item_price--sup">${p[1]}</sup>`;
  }

  renderTrades() {
    const trades = this.shops.map(({url, logo, name}) => {
      return `
        <a href="${url}" class="widget-buy-trade_item" target="_blank" rel="noreferrer">
          <img src="${logo}" alt="${name}">
        </a>
      `;
    }).join('');

    const element = document.createElement('div');
    element.innerHTML = `
      <div class="widget-buy-trade">
        <button class="widget-buy-trade_close ${this.closeClass}"></button>
        ${trades}
      </div>
    `;
    
    this.tradesEl = element.firstElementChild;
  }

  showTrades() {
    if (this.tradesEl) return;

    this.renderTrades();
    this.el.append(this.tradesEl);
  }

  closeTrades() {
    if (!this.tradesEl) return;

    this.tradesEl.remove();
    this.tradesEl = null
  }

  addEventListener() {
    this.el.addEventListener('click', (e) => {
      if (e.target.closest(`.${this.openClass}`)) {
        return this.showTrades();
      }
      if (e.target.closest(`.${this.closeClass}`)) {
        return this.closeTrades();
      }
    })
  }
}
