import "./Partners.scss";
import API from "./../../lib/HatchApi";

export default class Partners {
  color = {
    textColor: "#fff",
  };
  i18n = {
    empty: "No result",
  };

  constructor({
    color = {},
    params = {},
    i18n = {},
    noEmpty = false, //Не показывать блок "No result"
    page = 1,
    limit = 100,
  } = {}) {
    this.options = {
      color: Object.assign(this.color, color),
      params: params,
      noEmpty: noEmpty,
      i18n: Object.assign(this.i18n, i18n),
    };

    this.render();
    this.updateParams(this.options.params);
  }

  get template() {
    return `
      <section class="widget-partners" style="
        --textColor: ${this.options.color.textColor};
      "></div>
    `;
  }

  get templateEmpty() {
    return `
      <p class="widget-partners-empty">${this.i18n.empty}</p>
    `;
  }

  render() {
    const element = document.createElement("div");
    element.innerHTML = this.template;

    this.el = element.firstElementChild;
  }

  async renderPartners() {
    const responce = await this.api.get("partners", 0, 100);
    const $partners = responce.partners
      .map(({ name, logo, url }) => {
        return `
        <a href="${url}" class="widget-partners-item" target="_blank" rel="noreferrer" title="${name}">
          <img class="widget-partners-item_image" src="${logo}" alt="${name}" />
        </a>
      `;
      })
      .join("");

    this.el.innerHTML = $partners;

    this.el.dispatchEvent(
      new CustomEvent("widget-partners-responce", {
        detail: {
          products: responce.partners,
        },
        bubbles: false,
      })
    );

    if (responce.partners.length <= 0 && !this.options.noEmpty) {
      this.el.innerHTML = this.templateEmpty;
    }
  }

  /*
   * При изменении страны, серии или npm
   * достаточно передать 1 измененное свойство в updateParams
   */
  updateParams({ country, prod_type, series, mpn } = {}) {
    this.options.params.country = country || this.options.params.country;
    this.options.params.prod_type = prod_type || this.options.params.prod_type;
    this.options.params.series = series || this.options.params.series;
    this.options.params.mpn = mpn || this.options.params.mpn;

    this.api = new API(this.options.params);
    this.renderPartners(true);
  }
}
