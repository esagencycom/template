export { default as BuyWidget } from './widgets/Buy';
export { default as PartnersWidget } from './widgets/Partners';
export { default as PopupWidget } from "./widgets/Popup";