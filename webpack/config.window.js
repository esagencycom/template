import path from 'path';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';

export default (isDev) => {
  return {
    target: isDev ? 'web' : 'browserslist',
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, '../dist'),
      filename: 'window.build.js',
      libraryTarget: 'window'
    },
    optimization: {
      minimize: !isDev,
      minimizer: [
        `...`,
        new CssMinimizerPlugin()
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: 'window.build.css'
      }),
    ],
    module: {
      rules: [
        {
          test: /\.m?js$/,
          exclude: /node_modules\/(?!(template)\/).*/,
          use: {
            loader: 'babel-loader'
          },
        },
        {
          test: /\.s[ac]ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ]
        },
      ],
    }
  }
}
