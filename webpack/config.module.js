import path from 'path';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';

export default (isDev) => {
  return {
    target: isDev ? 'web' : 'browserslist',
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, '../dist'),
      filename: 'module.build.js',
      libraryTarget: 'module'
    },
    optimization: {
      minimize: !isDev
    },
    experiments: {
      outputModule: true,
    },
    module: {
      rules: [
        {
          test: /\.m?js$/,
          exclude: /node_modules\/(?!(template)\/).*/,
          use: {
            loader: 'babel-loader'
          },
        },
        {
          test: /\.s[ac]ss$/,
          use: [
            'style-loader',
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ]
        },
      ],
    }
  }
}
