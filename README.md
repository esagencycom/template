# Набор часто используемых виджетов

## Установка

Как NPM пакет:

```sh
npm i git+ssh://git@bitbucket.org/esagencycom/template.git --save
```

## Использование (JS module)

На примере виджета продуктов - BuyWidget

```js

import { BuyWidget } from 'template';

const Widget = new BuyWidget({
    params: {
        country: 'ru', // ru | uk | de | ...
        prod_type: 'ryzen', // ryzen | radeon
        series: 5000, // 3000 | 5000 | ....
        mpn: false,
    },
    i18n: {
        showMore: 'Показать еще',
        price: 'Цена <br> от: ',
        getYours: 'Где купить',
        empty: 'Нет данных',
    },
    color: {
        background: '#181818',
        textColor: '#fff',
        priceColor: '#878787',
        button: '#f26522',
        buttonHover: '#d54d0d',
        buttonColor: '#fff'
    },
    page: 0, 
    limit: 10,
});

document.body.apend( Widget.el );

```

Большинство аргументов виджетов имеют значения по умолчанию. 
i18n позволит указать собственные переводы - актуально для многоязычных сайтов.
color позволит гибко настроить цветовую схему виджета

## Использование (JS window)

Скачиваем файлы window.build.css, window.build.js
И подключаем файлы к верстке:

```html

<link rel="stylesheet" href="_path_/window.build.css">
<script src="_path_/window.build.js"></script>

```

Все виджеты будут доступны в объекте window. 

```js

const BuyWidget = window.BuyWidget;
const Widget = new BuyWidget(....);

```

## Список виджетов:

- BuyWidget - виджет показа продуктов (demo - dist/demo/buy-widget.html)
- PartnersWidget - виджет показа партнеров (demo - dist/demo/partners-widget.html)
- PopupsWidget - виджет показа модального окна (demo - dist/demo/popup-widget.html)