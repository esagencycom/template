module.exports = {
  plugins: [
    require('postcss-animation'),
    require('autoprefixer')({
      cascade: false
    }),
    require('postcss-preset-env')
  ]
}