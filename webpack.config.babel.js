import configWindow from './webpack/config.window.js';
import configModule from './webpack/config.module.js';

export default (env, argv) => {
  const isDev = argv.mode === 'development';
  
  return [
    configWindow(isDev),
    configModule(isDev),
  ]
}
