## 1.0.9 (2021-06-19)

- Bug fix: image size for partners

## 1.0.8 (2021-06-04)

- Bug fix: Added arg name "models" for API

## 1.0.7 (2021-06-04)

- Added CustomEvent for BuyWidget and PartnersWidget
- Remove count responce for BuyWidget and PartnersWidget

## 1.0.6 (2021-06-02)

- Added count responce for BuyWidget and PartnersWidget

## 1.0.5 (2021-06-01)

- Autoplay youtube video for PopupWidget

## 1.0.4 (2021-06-01)

- Fixed PartnersWidget
- Added PopupWidget
- Update demo

## 1.0.3 (2021-05-05)

- Fixed work with pagination, added page and limit arguments
- Added empty message in i18n arguments
- Update demo BuyWidget

## 1.0.2 (2021-05-05)

- Fixed generate module and added README.md

## 1.0.1 (2021-05-03)

- NPM module

## 1.0.0 (2021-04-19)

- Initial release
